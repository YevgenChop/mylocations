import React, { useEffect, useRef, useState } from 'react';
import { LatLng, parseLocation } from 'utils/parseLocation';

interface LocationPickerProps {
  onChange: (value: string) => void;
  value?: string;
}

const defaultLocation: LatLng = {
  lat: -22.363,
  lng: 121.044,
};

const LocationPicker: React.FC<LocationPickerProps> = ({ onChange, value }) => {
  const mapRef = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<google.maps.Map>();
  const [marker, setMarket] = useState<google.maps.Marker>();

  useEffect(() => {
    if (mapRef.current) {
      const center = value ? parseLocation(value) : defaultLocation;

      const map = new google.maps.Map(mapRef.current, {
        zoom: 6,
        center,
      });

      setMap(map);
    }
  }, [mapRef]);

  useEffect(() => {
    if (value && map && !marker) {
      const position = parseLocation(value);

      setMarket(new google.maps.Marker({
        position,
        map,
        draggable: true,
      }));
    }
  }, [map, marker]);

  useEffect(() => {
    if (map) {
      const listener = google.maps.event.addListener(map, 'click', ({ latLng }) => {
        if (latLng) {
          onChange(`${latLng.lat()},${latLng.lng()}`);
          if (!marker) {
            setMarket(new google.maps.Marker({
              position: latLng,
              map,
              draggable: true,
            }));

          } else {
            marker.setPosition(latLng);
          }
        }
      });

      return () => google.maps.event.removeListener(listener);
    }
  }, [map, marker]);

  useEffect(() => {
    if (marker) {
      const listener = google.maps.event.addListener(marker, 'dragend', ({ latLng }) => {
        onChange(`${latLng.lat()},${latLng.lng()}`);
        marker.setPosition(latLng);
      });

      return () => google.maps.event.removeListener(listener);
    }
  }, [marker]);

  return (
    <div ref={mapRef} style={{ height: 400 }}/>
  );
};

export default LocationPicker;