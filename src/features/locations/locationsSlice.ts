import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { nanoid } from 'nanoid';
import { LocationDto } from './CreateEditLocation';

interface Location {
  id: string;
  name: string;
  address: string;
  coordinates: string;
  category: string;
}

export type SortType = 'desc' | 'asc';

export interface Order {
  sortColumn: string,
  sortType: SortType,
}

export interface LocationsSlice {
  list: Location[],
  order: Order,
  filter: string[],
}

const defaultLocations: Location[] = [
  {
    id: nanoid(5),
    name: 'island',
    address: 'address',
    coordinates: '64.9638740246858,-19.131733891572882',
    category: '1',
  },
  {
    id: nanoid(5),
    name: 'Canada',
    address: 'address2',
    coordinates: '51.18459932942563,-86.62684087061893',
    category: '2',
  },
];

const initialState = {
  list: defaultLocations.slice(),
  order: {
    sortColumn: '',
    sortType: 'asc',
  },
  filter: [],
} as LocationsSlice;


const locationsSlice = createSlice({
  name: 'locations',
  initialState,
  reducers: {
    add(state, action: PayloadAction<LocationDto>) {
      state.list.push({
        ...action.payload,
        id: nanoid(6),
      });
    },
    remove(state, action: PayloadAction<string>) {
      state.list = state.list.filter(location => location.id !== action.payload);
    },

    edit(state, action: PayloadAction<Location>) {
      state.list = state.list.map(location => {
        if (location.id === action.payload.id) {
          return action.payload;
        }
        return location;
      });
    },
    setSort(state, action: PayloadAction<Order>){
        state.order = action.payload
    },
    setFilter(state, action: PayloadAction<string[]>){
      state.filter = action.payload
    }
  },
});

export const locationsActions = locationsSlice.actions;
export default locationsSlice.reducer;