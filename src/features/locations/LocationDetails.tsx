import React, { useEffect, useRef } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectLocationById } from './locationsSelector';
import { Col, Row, Container, Grid, Panel, Content } from 'rsuite';
import { parseLocation } from 'utils/parseLocation';

interface LocationDetailsProps {

}

const LocationDetails: React.FC<LocationDetailsProps> = () => {
  const { id } = useParams<{ id: string }>();
  const location = useSelector(selectLocationById(id));
  const mapRef = useRef<HTMLDivElement>(null);

  const vibrateOnClick = () => {
    if (typeof window.navigator.vibrate === 'function') {
      window.navigator.vibrate(200)
    }
  };

  useEffect(() => {
    if (mapRef.current && location) {
      const position = parseLocation(location.coordinates);
      const map = new google.maps.Map(mapRef.current, {
        zoom: 6,
        center: position,
      });
      new google.maps.Marker({
        position,
        map,
        title: location.name,
      });
    }

  }, [mapRef, location]);

  if (!location) {
    return <div>Not found</div>;
  }

  return (
    <Container>
      <Content style={{ padding: 15 }}>
        <Panel bordered header={location.name}>
          <Grid fluid>
            <Row>
              <Col xs={6}>Address</Col>
              <Col xs={6}>{location.address}</Col>
            </Row>

            <Row>
              <Col xs={6} onClick={vibrateOnClick}>Coordinates</Col>
              <Col xs={6}>{location.coordinates}</Col>
            </Row>

            <Row>
              <Col xs={6}>Category</Col>
              <Col xs={6}>{location.category.name}</Col>
            </Row>
          </Grid>
        </Panel>
        <Panel>
          <div ref={mapRef} style={{ height: 500 }}/>
        </Panel>
      </Content>
    </Container>
  );
};

export default LocationDetails;