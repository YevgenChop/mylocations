import React, { useState } from 'react';
import { Button, CheckPicker, Container, Content, Icon, IconButton, Modal, Nav, Navbar, Table, Toggle } from 'rsuite';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { RootState } from 'app/rootReducer';
import { Category } from '../categories/categoriesSlice';
import CreateEditLocation from './CreateEditLocation';
import { locationsActions, Order, SortType } from './locationsSlice';
import useModal from 'hooks/useModal';
import { selectFilter, selectLocationOrder, selectLocationSortedList } from './locationsSelector';
import { MarkersMap, Title } from 'components';
import { selectCategoriesList } from '../categories/categoriesSelectors';

const { Cell, Column, HeaderCell } = Table;

export interface Location {
  id: string;
  name: string;
  address: string;
  coordinates: string;
  category: Category;
}

const Locations: React.FC<RouteComponentProps> = ({ history }) => {

  const dispatch = useDispatch();

  const [showMap, setShowMap] = useState<boolean>(false);
  const categories = useSelector<RootState, Category[]>(selectCategoriesList);
  const createEditModal = useModal(false);
  const deleteModal = useModal(false);

  const [selected, setSelected] = useState<Location | undefined>();

  const locations = useSelector<RootState, Location[]>(selectLocationSortedList);
  const order = useSelector<RootState, Order>(selectLocationOrder);
  const filter = useSelector<RootState, string[]>(selectFilter);

  const onAddNew = () => {
    setSelected(undefined);
    createEditModal.open();
  };

  const handleSortColumn = (sortColumn: string, sortType: SortType) => {
    dispatch(locationsActions.setSort({
      sortColumn,
      sortType,
    }));
  };

  return (
    <Container>
      <Content>
        <Navbar>
          <Title value="Locations"/>
          <Navbar.Body>
            <CheckPicker
              data={categories}
              labelKey="name"
              valueKey="id"
              placeholder="Select category"
              style={{ width: 224, padding: 10 }}
              value={filter}
              onChange={(values) => dispatch(locationsActions.setFilter(values))}
            />
            <Toggle
              size="lg"
              checkedChildren="Hide map"
              unCheckedChildren="Show on map"
              checked={showMap}
              onChange={setShowMap}
            />
            <Nav pullRight>
              <Nav.Item onClick={onAddNew} icon={<Icon icon="plus"/>}>Add new</Nav.Item>
            </Nav>
          </Navbar.Body>
        </Navbar>
        <Table
          height={window.innerHeight * 0.4}
          data={locations}
          sortColumn={order.sortColumn}
          sortType={order.sortType}
          onSortColumn={handleSortColumn}
          onRowClick={(row) => history.push(`/locations/${row.id}`)}
        >
          <Column width={80} align="center" fixed>
            <HeaderCell>Id</HeaderCell>
            <Cell dataKey="id"/>
          </Column>

          <Column flexGrow={1} sortable>
            <HeaderCell>name</HeaderCell>
            <Cell dataKey="name"/>
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>address</HeaderCell>
            <Cell dataKey="address"/>
          </Column>

          <Column flexGrow={1} sortable>
            <HeaderCell>Category</HeaderCell>
            <Cell dataKey="category">{(row: Location) => row.category.name}</Cell>
          </Column>

          <Column width={100} fixed="right">
            <HeaderCell>Action</HeaderCell>
            <Cell style={{ padding: 5 }}>
              {(rowData: Location) => {
                return (
                  <span onClick={(e) => e.stopPropagation()}>
                  <IconButton
                    appearance="subtle"
                    onClick={() => {
                      createEditModal.open();
                      setSelected(rowData);
                    }}
                    icon={<Icon icon="edit2"/>}
                  />
                  <IconButton
                    appearance="subtle"
                    onClick={() => {
                      deleteModal.open();
                      setSelected(rowData);
                    }}
                    icon={<Icon icon="trash"/>}
                  />
                </span>
                );
              }}
            </Cell>
          </Column>
        </Table>
        {showMap && <MarkersMap locations={locations} height={window.innerHeight * 0.51}/>}
      </Content>
      <Modal backdrop="static" show={deleteModal.show} onHide={deleteModal.close} size="xs">
        <Modal.Body>
          <Icon
            icon="remind"
            style={{
              color: '#ffb300',
              fontSize: 24,
            }}
          />
          You want to delete {selected?.name},
          are you sure you want to proceed?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => {
            deleteModal.close();
            if (selected) {
              dispatch(locationsActions.remove(selected.id));
            }
          }} appearance="primary">
            Ok
          </Button>
          <Button onClick={deleteModal.close} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      <CreateEditLocation
        categories={categories}
        show={createEditModal.show}
        close={createEditModal.close}
        onSubmit={(values) => {
          if (selected) {
            dispatch(locationsActions.edit({
              id: selected.id,
              ...values,
            }));

          } else {
            dispatch(locationsActions.add(values));
          }
          createEditModal.close();
        }}
        data={selected ? {
          ...selected,
          category: selected?.category.id || '',
        } : undefined}
      />

    </Container>
  );
};

export default withRouter(Locations);
