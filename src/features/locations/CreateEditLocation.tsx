import React from 'react';
import { Button, ControlLabel, ErrorMessage, Form, FormControl, FormGroup, Modal, Panel, SelectPicker } from 'rsuite';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { Category } from '../categories/categoriesSlice';
import LocationPicker from './LocationPicker';

export interface LocationDto {
  id?: string;
  name: string;
  address: string;
  coordinates: string;
  category: string;
}

interface CreateLocationProps {
  show: boolean;
  close: () => void;
  onSubmit: (values: LocationDto) => void,
  data?: LocationDto,
  categories: Category[]
}

const validationSchema = yup.object({
  name: yup
    .string()
    .min(3, 'Name should be of minimum 3 characters length')
    .required('Name is required'),

  address: yup
    .string()
    .min(3, 'Name should be of minimum 3 characters length')
    .required('Address is required'),

  coordinates: yup
    .string()
    .min(3, 'Name should be of minimum 3 characters length')
    .required('Coordinates is required'),

  category: yup
    .string()
    .required('Category is required'),

});

const CreateEditLocation: React.FC<CreateLocationProps> = ({ data, show, close, onSubmit, categories }) => {
  const formik = useFormik<LocationDto>({
    initialValues: {
      name: data?.name || '',
      address: data?.address || '',
      coordinates: data?.coordinates || '',
      category: data?.category || '',
    },
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit,
  });

  const getError = (field: keyof LocationDto) => {
    return formik.errors[field] && formik.touched[field] ? formik.errors[field] : '';
  };

  return (
    <Modal backdrop="static" show={show} onHide={close} size="sm">
      <Modal.Body>
        <Form layout="horizontal">
          <FormGroup>
            <ControlLabel>Name</ControlLabel>
            <FormControl
              name="name"
              value={formik.values.name}
              onChange={(value, event) => formik.handleChange(event)}
              errorMessage={getError('name')}
            />
          </FormGroup>

          <FormGroup>
            <ControlLabel>Address</ControlLabel>
            <FormControl
              name="address"
              value={formik.values.address}
              onChange={(value, event) => formik.handleChange(event)}
              errorMessage={getError('address')}
            />
          </FormGroup>

          <FormGroup>
            <ControlLabel>Coordinates</ControlLabel>
            <FormControl
              name="coordinates"
              value={formik.values.coordinates}
              onChange={(value, event) => formik.handleChange(event)}
              errorMessage={getError('coordinates')}
            />
          </FormGroup>

          <FormGroup>
            <ControlLabel>Category</ControlLabel>
            <div className="rs-form-control-wrapper">
              <SelectPicker
                labelKey='name'
                valueKey="id"
                data={categories}
                style={{ width: 224 }}
                value={formik.values.category}
                onChange={(value) => {
                  formik.handleChange({
                    target: {
                      name: 'category',
                      value,
                    },
                  });
                }}
              />
              <ErrorMessage show={Boolean(getError('category'))} placement="bottomStart">
                {getError('category')}
              </ErrorMessage>
            </div>
          </FormGroup>
        </Form>

        <Panel>
          <LocationPicker
            value={formik.values.coordinates}
            onChange={(value) => formik.setFieldValue('coordinates', value)}
          />
        </Panel>

      </Modal.Body>
      <Modal.Footer>
        <Button onClick={formik.submitForm} appearance="primary">
          Save
        </Button>
        <Button onClick={close} appearance="subtle">
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>

  );
};

export default CreateEditLocation;