import { createSelector } from '@reduxjs/toolkit';
import { selectCategoriesList } from 'features/categories/categoriesSelectors';
import { RootState } from 'app/rootReducer';

const selectLocationsRoot = (state: RootState) => state.locations;

export const selectLocationList = createSelector(
  [selectLocationsRoot, selectCategoriesList],
  (state, categories) => {
    return state.list.map((location => {
      const category = categories.find(category => category.id === location.category) || { id: '', name: '' };
      return {
        ...location,
        category,
      };
    }));
  },
);

export const selectFilter = createSelector(selectLocationsRoot, (state) => state.filter);

export const selectLocationById = (id: string) => createSelector(
  selectLocationList,
  (locations) => {
    return locations.find((location => location.id === id));
  },
);

export const selectLocationOrder = createSelector(
  selectLocationsRoot,
  state => state.order,
);

export const selectLocationsByFilter = createSelector([selectLocationList, selectFilter], (locations, filter) => {
  if (filter.length) {
    return locations.filter(({ category }) => filter.includes(category.id));
  }

  return locations;
});

export const selectLocationSortedList = createSelector(
  [selectLocationsByFilter, selectLocationOrder],
  (list, { sortColumn, sortType }) => {
    if (!sortColumn) {
      return list;
    }

    return list.sort((a, b) => {
      const isAsc = sortType === 'asc';

      // @ts-ignore
      const left = typeof a[sortColumn] === 'object' ? a[sortColumn].name : a[sortColumn];
      // @ts-ignore
      const right = typeof b[sortColumn] === 'object' ? b[sortColumn].name : b[sortColumn];

      if (left < right) {
        return isAsc ? -1 : 1;
      }

      if (left > right) {
        return isAsc ? 1 : -1;
      }
      return 0;
    });
  });
