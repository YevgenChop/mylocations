import { createSelector } from "@reduxjs/toolkit";
import { RootState } from '../../app/rootReducer';

const selectLocationsRoot = (state: RootState) => state.categories;

export const selectCategoriesList = createSelector(
  selectLocationsRoot,
  state => state.list
);

