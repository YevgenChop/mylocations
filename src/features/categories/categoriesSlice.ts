import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { nanoid } from 'nanoid';

export interface Category {
  id: string;
  name: string;
}

interface CategoriesSlice {
  list: Category[]
}

const defaultCategories: Category[] = [
  {
    id: '1',
    name: 'Favorite',
  },
  {
    id: '2',
    name: 'Best',
  },
  {
    id: '3',
    name: 'Future',
  },
];

const initialState = {
  list: defaultCategories.slice(),
} as CategoriesSlice;

const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {
    add(state, action: PayloadAction<string>) {
      state.list.push({
        id: nanoid(6),
        name: action.payload,
      });
    },
    remove(state, action: PayloadAction<string>) {
      state.list = state.list.filter(category => category.id !== action.payload);
    },

    edit(state, action: PayloadAction<Category>) {
      state.list = state.list.map(category => {
        if (category.id === action.payload.id) {
          return action.payload;
        }
        return category;
      });
    },
  },
});

export const categoriesActions = categoriesSlice.actions;
export default categoriesSlice.reducer;