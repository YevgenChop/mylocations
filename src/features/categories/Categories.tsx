import {
  Button,
  Container,
  Content,
  Icon,
  IconButton,
  Modal,
  Nav,
  Navbar,
  Table,
} from 'rsuite';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useModal from 'hooks/useModal';
import { categoriesActions, Category } from './categoriesSlice';
import { RootState } from 'app/rootReducer';
import CreateEditCategory from './CreateEditCategory';
import { Title } from 'components';

const { Cell, Column, HeaderCell } = Table;

const Categories = () => {
  const deleteModal = useModal(false);
  const createEditModal = useModal(false);
  const [selected, setSelected] = useState<Category | undefined>();
  const dispatch = useDispatch();

  const list = useSelector<RootState, Category[]>(state => state.categories.list);

  const onAddNew = () => {
    createEditModal.open();
    setSelected(undefined);
  };

  return (
    <Container>
      <Content>
        <Navbar>
          <Title value="Categories"/>
          <Navbar.Body>
            <Nav pullRight>
              <Nav.Item onClick={onAddNew} icon={<Icon icon="plus"/>}>Add new</Nav.Item>
            </Nav>
          </Navbar.Body>
        </Navbar>
        <Table height={400} data={list}>
          <Column width={150} align="center" fixed>
            <HeaderCell>Id</HeaderCell>
            <Cell dataKey="id"/>
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>name</HeaderCell>
            <Cell dataKey="name"/>
          </Column>

          <Column width={100} fixed="right">
            <HeaderCell>Action</HeaderCell>
            <Cell style={{ padding: 5 }}>
              {(rowData: Category) => {
                return (
                  <span>
                  <IconButton
                    appearance="subtle"
                    onClick={() => {
                      createEditModal.open();
                      setSelected(rowData);
                    }}
                    icon={<Icon icon="edit2"/>}
                  />
                  <IconButton
                    appearance="subtle"
                    onClick={() => {
                      deleteModal.open();
                      setSelected(rowData);
                    }}
                    icon={<Icon icon="trash"/>}
                  />
                </span>
                );
              }}
            </Cell>
          </Column>
        </Table>
      </Content>
      <Modal backdrop="static" show={deleteModal.show} onHide={deleteModal.close} size="xs">
        <Modal.Body>
          <Icon
            icon="remind"
            style={{
              color: '#ffb300',
              fontSize: 24,
            }}
          />
          You want to delete {selected?.name},
          are you sure you want to proceed?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => {
            deleteModal.close();
            if (selected) {
              dispatch(categoriesActions.remove(selected.id));
            }
          }} appearance="primary">
            Ok
          </Button>
          <Button onClick={deleteModal.close} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      <CreateEditCategory
        data={selected}
        show={createEditModal.show}
        onSubmit={(values) => {
          if (selected) {
            dispatch(categoriesActions.edit({
              id: selected.id,
              name: values.name,
            }));

          } else {
            dispatch(categoriesActions.add(values.name));
          }
          createEditModal.close();
        }}
        close={createEditModal.close}/>
    </Container>
  );

};

export default Categories;