import React from 'react';
import { Button, ControlLabel, Form, FormControl, FormGroup, Modal } from 'rsuite';
import { useFormik } from 'formik';
import * as yup from 'yup';

interface CategoryDto {
  id?: string;
  name: string
}

interface CreateCategoryProps {
  show: boolean;
  close: () => void;
  onSubmit: (values: CategoryDto) => void,
  data?: CategoryDto
}

const validationSchema = yup.object({
  name: yup
    .string()
    .min(3, 'Name should be of minimum 3 characters length')
    .required('Name is required'),
});

const CreateEditCategory: React.FC<CreateCategoryProps> = ({ data, show, close, onSubmit }) => {

  const formik = useFormik<CategoryDto>({
    initialValues: {
      name: data?.name || '',
    },
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit,
  });

  return (
    <Modal backdrop="static" show={show} onHide={close} size="xs">
      <Modal.Body>
        <Form layout="vertical">
          <FormGroup>
            <ControlLabel>Name</ControlLabel>
            <FormControl
              name="name"
              value={formik.values.name}
              onChange={(value, event) => formik.handleChange(event)}
              errorMessage={ formik.errors.name && formik.touched.name ? formik.errors.name : '' }
              errorPlacement="bottomStart"
            />
          </FormGroup>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={formik.submitForm} appearance="primary">
          Save
        </Button>
        <Button onClick={close} appearance="subtle">
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>

  );
};

export default CreateEditCategory;
