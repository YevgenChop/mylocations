import { combineReducers } from '@reduxjs/toolkit'
import categoriesSlice from 'features/categories/categoriesSlice';
import locationsSlice from 'features/locations/locationsSlice';

const rootReducer = combineReducers({
  categories: categoriesSlice,
  locations: locationsSlice
});

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer