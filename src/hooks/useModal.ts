import { useState } from 'react';

export interface useModalState {
  show: boolean,
  open: () => void,
  close: () => void,
}

const useModal = (initial: boolean = false): useModalState => {
  const [show, setShow] = useState<boolean>(initial);

  const open = () => setShow(true);

  const close = () => setShow(false);

  return {
    show,
    open,
    close
  }
};

export default useModal;