import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { Container, Content } from 'rsuite';

import Navigation from 'components/Navigation';
import Categories from 'features/categories/Categories';
import Locations from 'features/locations/Locations';
import LocationDetails from 'features/locations/LocationDetails';

const App = () => {
  return (
    <Router>
      <Container>
        <Container style={{ minHeight: 'calc(100vh - 36px)' }}>
          <Content>
            <Switch>
              <Route path="/locations/:id">
                <LocationDetails/>
              </Route>
              <Route path="/locations">
                <Locations/>
              </Route>
              <Route path="/categories">
                <Categories/>
              </Route>
              <Redirect to="/categories"/>
            </Switch>
          </Content>
        </Container>
        <Navigation/>

      </Container>
    </Router>
  );
};

export default App;
