export interface LatLng {
  lat: number;
  lng: number;
}
export const parseLocation = (coordinates: string): LatLng => {
  const [lat, lng] = coordinates.split(',').map((str) => Number(str.trim()));
  return { lat, lng };
};