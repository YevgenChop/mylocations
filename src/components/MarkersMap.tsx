import React, { useEffect, useRef, useState } from 'react';
import { parseLocation } from '../utils/parseLocation';

interface MarkersMapProps {
  locations: {
    coordinates: string,
    name: string
  }[],
  height?: number
}

const MarkersMap: React.FC<MarkersMapProps> = ({ locations, height= 400 }) => {
  const mapRef = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<google.maps.Map>();
  const [markers, setMarkers] = useState<google.maps.Marker[]>([]);
  useEffect(() => {
    if (mapRef.current) {

      const map = new google.maps.Map(mapRef.current, {
        zoom: 6,
        center: {
          lat: -22.363,
          lng: 121.044,
        },
      });

      setMap(map);
    }
  }, [mapRef]);

  useEffect(() => {
    if (map && locations.length) {
      markers.forEach(marker => marker.setMap(null));

      const newMarkers = locations.map( location => {
        const position = parseLocation(location.coordinates);

        return new google.maps.Marker({
          position,
          map,
          draggable: true,
          title: location.name,
        })
      });

      setMarkers(newMarkers);

      const bounds = new google.maps.LatLngBounds();
      newMarkers.forEach(marker => {
        const position = marker.getPosition();
        if (position) {
          bounds.extend(position)
        }
      });
      map.fitBounds(bounds);
    }
  }, [locations, map]);

  return <div ref={mapRef} style={{ height }}/>;

};

export default MarkersMap;
