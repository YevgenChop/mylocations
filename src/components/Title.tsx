import { Navbar } from 'rsuite';
import React from 'react';
import styled from 'styled-components';

interface TitleProps {
  value: string
}

const Title: React.FC<TitleProps> = ({ value }) => (
  <Navbar.Header>
    <Container>
      {value}
    </Container>
  </Navbar.Header>

);

const Container = styled.div`
  padding: 10px;
  font-size: 24px;
  font-weight: 600;
  height: 100%;
`;

export default Title;