import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Button, ButtonGroup, Icon } from 'rsuite';
import { SVGIcon } from 'rsuite/lib/@types/common';
import { IconNames } from 'rsuite/lib/Icon/Icon';

interface RouteItem {
  path: string;
  icon: IconNames | SVGIcon;
  label: string;
}

const routes: RouteItem[] = [
  {
    path: '/categories',
    icon: 'list',
    label: 'Categories',
  },
  {
    path: '/locations',
    icon: 'location-arrow',
    label: 'Locations',
  },

];

const Navigation: React.FC<RouteComponentProps> = ({ history, location }) => {
  return (
    <ButtonGroup justified>
      {routes.map((route) => {
        const isActive = location.pathname.includes(route.path);
        const onClick = () => history.push(route.path);
        return (
          <Button key={route.path} appearance={isActive ? 'primary' : 'ghost'} onClick={onClick}>
            <Icon icon={route.icon}/>
            <span style={{ marginLeft: 10 }}>
              { route.label }
            </span>
          </Button>
        );
      })}
    </ButtonGroup>
  );
};
export default withRouter(Navigation);
